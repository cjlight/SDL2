﻿#include <SDL.h>
#include <SDL_thread.h>

// Data access semaphore
SDL_SpinLock gDataLock = 0;

// Target delta times in ms
const Uint32 gUpdateTargetDelta = 1000 / 30;
const Uint32 gRenderTargetDelta = 1000 / 60;
const Uint32 gClearTargetDelta = 1000 / 0.5;

// Window dimensions
const unsigned int gWindowX = 320;
const unsigned int gWindowY = 160;

// Green Square position
int gX = 0, gY = 0;

// Green Square size
const unsigned int gSize = 16;

// Green Square velocity
const unsigned int gVel = 4;

// Update flags
bool isAlive = true;
bool isRender = true;
bool isUpdate = true;

// Graphics renderer
SDL_Renderer* gRenderer;

// Event handler
SDL_Event gEvent;

// Array of the keys pressed
bool gKeysDown[256];

// Update thread
int update(void* data)
{
	// Loop in the thread until we trigger its end
	while (isAlive && isUpdate)
	{
		if (gKeysDown[SDLK_a])
		{
			gX -= gVel;
		}
		else if (gKeysDown[SDLK_d])
		{
			gX += gVel;
		}

		if (gKeysDown[SDLK_s])
		{
			gY += gVel;
		}
		else if (gKeysDown[SDLK_w])
		{
			gY -= gVel;
		}

		// Clamp the green square position
		gX = (gX < 0) ? 0 : (gX > gWindowX - gSize) ? gWindowX - gSize : gX;
		gY = (gY < 0) ? 0 : (gY > gWindowY - gSize) ? gWindowY - gSize : gY;

		// Delay to the update target
		SDL_Delay(gUpdateTargetDelta);
	}

	return 0;
}

// Draw thread
int renderClear(void* data)
{
	// Loop in the thread until we trigger its end
	while (isAlive && isRender)
	{
		// Lock to stop frame out of sync
		SDL_AtomicLock(&gDataLock);

		// Clear back buffer light black
		SDL_SetRenderDrawColor(gRenderer, 0x09, 0x09, 0x09, 0x22);
		SDL_RenderClear(gRenderer);

		// Unlock
		SDL_AtomicUnlock(&gDataLock);

		SDL_Delay(gClearTargetDelta);
	}

	return 0;
}

// Draw thread
int draw(void* data)
{
	// Loop in the thread until we trigger its end
	while (isAlive && isRender)
	{
		// Render our green square
		const SDL_Rect greenRect = { gX, gY, gSize, gSize };

		SDL_SetRenderDrawColor(gRenderer, 0, 255, 0, 255);
		SDL_RenderFillRect(gRenderer, &greenRect);

		// Swap back buffer to front
		SDL_RenderPresent(gRenderer);

		// Delay to the render target
		SDL_Delay(gRenderTargetDelta);
	}

	return 0;
}

int main(int argc, char *args[])
{
	// Create window 
	SDL_Window* window = SDL_CreateWindow(
		"Example03", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gWindowX, gWindowY, SDL_WINDOW_SHOWN);

	// Create renderer for window
	gRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	// Create the threads
	SDL_Thread* threadUpdate = SDL_CreateThread(update, "update", NULL);
	SDL_Thread* threadDrawClear = SDL_CreateThread(renderClear, "draw", NULL);
	SDL_Thread* threadDraw = SDL_CreateThread(draw, "draw", NULL);

	while (isAlive)
	{
		while (SDL_PollEvent(&gEvent) != 0)
		{
			switch (gEvent.type)
			{
				// Stop SDL if the window is closed
			case SDL_QUIT:
				isAlive = false;
				break;
				// Check for if a key is pressed
			case SDL_KEYDOWN:
				if (gEvent.key.keysym.sym < 255)
				{
					gKeysDown[gEvent.key.keysym.sym] = true;
				}
				break;
				// Check if a key is released
			case SDL_KEYUP:
				if (gEvent.key.keysym.sym < 255)
				{
					gKeysDown[gEvent.key.keysym.sym] = false;
				}
				break;
			}
		}
	}

	// Only continue of these threads have been finished
	SDL_WaitThread(threadUpdate, NULL);
	SDL_WaitThread(threadDrawClear, NULL);
	SDL_WaitThread(threadDraw, NULL);

	// Release SDL
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(gRenderer);
	SDL_Quit();

	return 0;
}